#!/bin/bash
cp ../chapters/*.quarto_ipynb .
for i in *.quarto_ipynb 
    do
      echo $i
      mv $i "${i%.quarto_ipynb}.ipynb"
      jupyter nbconvert --inplace --clear-output --to notebook "${i%.quarto_ipynb}.ipynb"
    done  
