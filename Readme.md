# Julia für Numerik

Source code for the website and PDF script ["Julia für Numerik"](https://www.math.uni-leipzig.de/~hellmund/juliabook/)

- It uses the [Quarto publishing system](https://quarto.org/).
- It uses the  Quarto extension [julia-color](https://github.com/MHellmund/julia-color) to support
the conversion of Jupyter code output cells with ANSI escape sequences to HTML and LaTeX/PDF

All content & code licenced under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
