::: {#nb1 .cell execution_count=1}
``` {.julia .cell-code}
function show_subtype_tree(T, i=0)
    println("       "^i, T)
    for Ts ∈ subtypes(T)
        show_subtype_tree(Ts, i+1)
    end
end

show_subtype_tree(Number)
```

::: {.cell-output .cell-output-stdout}
```
Number
       Complex
       Real
              AbstractFloat
                     BigFloat
                     Float16
                     Float32
                     Float64
              AbstractIrrational
                     Irrational
              Integer
                     Bool
                     Signed
                            BigInt
                            Int128
                            Int16
                            Int32
                            Int64
                            Int8
                     Unsigned
                            UInt128
                            UInt16
                            UInt32
                            UInt64
                            UInt8
              Rational
```
:::
:::


::: {#nb2 .cell execution_count=2}
``` {.julia .cell-code}
using AbstractTrees

AbstractTrees.children(x::Type) = subtypes(x)
print_tree(Number)
```

::: {.cell-output .cell-output-stdout}
```
Number
├─ Complex
└─ Real
   ├─ AbstractFloat
   │  ├─ BigFloat
   │  ├─ Float16
   │  ├─ Float32
   │  └─ Float64
   ├─ AbstractIrrational
   │  └─ Irrational
   ├─ Integer
   │  ├─ Bool
   │  ├─ Signed
   │  │  ├─ BigInt
   │  │  ├─ Int128
   │  │  ├─ Int16
   │  │  ├─ Int32
   │  │  ├─ Int64
   │  │  └─ Int8
   │  └─ Unsigned
   │     ├─ UInt128
   │     ├─ UInt16
   │     ├─ UInt32
   │     ├─ UInt64
   │     └─ UInt8
   └─ Rational
```
:::
:::



