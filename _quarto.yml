project:
  type: book
  resources:
    - fonts/
    - images/

lang: de
language: 
  title-block-published: "Updated"
  title-block-author-single: "Autor"
  toc-title-document: "Auf dieser Seite"
  
book:
  title: "Julia für Numerik"
  subtitle: "Einführung in die wissenschaftliche Programmierung" 
  author: "Meik Hellmund"
  site-url:  "https://www.math.uni-leipzig.de/~hellmund/juliabook"
  favicon: "images/favicon-32x32.png"
  
  sidebar:
    logo: "images/julia-logo-color.svg"
    tools:
      - icon: github
        href: https://gitea.math.uni-leipzig.de/hellmund/JuliaKurs23
      - icon: download
        href: "Julia-f%C3%BCr-Numerik.pdf"
      

  search: true
  downloads: [pdf]
  date: today
  page-footer:
    left: |
      All content licenced under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
    right: |
      Created with [Quarto](https://quarto.org/)  
    background: "#f8f9fa"
  page-navigation: true
  back-to-top-navigation: true   

  chapters:
    - index.qmd
    - chapters/entwicklungsumgebungen.qmd
    - chapters/first_contact.qmd
    - chapters/Erste_Bsp.qmd
    - chapters/syntax.qmd
    - chapters/5_TricksHelp.qmd
    - chapters/numerictypes.qmd
    - chapters/Pi2.qmd
    - chapters/types.qmd
    - chapters/pcomplex.qmd
    - chapters/9_functs.qmd
    - chapters/6_ArraysEtcP1.qmd
    - chapters/7_ArraysP2.qmd
    - chapters/11_LinAlg.qmd
    - chapters/10_Strings.qmd
    - chapters/13_IO.qmd  
    - chapters/14_Plot.qmd
  #  - chapters/makie.qmd


format:
  julia-color-html:
    theme:
      light: [cosmo,     css/light.scss] 
      dark:  [superhero, css/dark.scss]
    css:
      - css/styles.css
      - css/roboto-condensed.css
      
    highlight-style: github # arrow ?????
    mainfont: "Roboto Condensed" # Ubuntu # "Atkinson Hyperlegible" # Verdana  #Quicksand
    toc: true   # change for Vorlesung
    toc-depth: 3
    number-depth: 3
    email-obfuscation: javascript
    smooth-scroll: false
    grid:
      sidebar-width:	250px   
      body-width:	1150px # 800
      margin-width:	250px
      gutter-width: 1.5em
    cap-location: bottom
    html-math-method: mathjax

  julia-color-pdf:
    pdf-engine: xelatex
    papersize: a4
    documentclass: scrreprt
    classoption: [notitlepage,DIV=16,BCOR=5mm,10pt]
    toc: false
    number-depth: 2
    fig-cap-location: bottom
    fig-pos: H
    tbl-cap-location: bottom
    hyperrefoptions:
      - linktoc=all
    colorlinks: true
    linkcolor: "blue"
    keep-tex: true
    keep-ipynb: true
    keep-md: true
    code-block-bg: "#e0e0e0"
    code-block-border-left: "#e0e0e0"
    code-overflow: wrap
    include-in-header:
      - file: macros.tex
      
latex-auto-install: false

execute:
  cache: true
  output: true
  echo: true
  warning: true
  error: true
  eval: true
  freeze: auto
  daemon: 3600
  preserve-ansi: true

keep-ipynb: true
keep-tex: true
keep-md: true

jupyter: julia-1.10

filters: 
  - code-visibility
  - latex-environment

environments: [quote, indentb]
